package ru.t1.lazareva.tm.api.service;

import ru.t1.lazareva.tm.model.Task;

public interface IProjectTaskService {

    Task bindTaskToProject(String projectId, String taskId);

    void removeProjectById(String projectId);

    Task unbindTaskFromProject(String projectId, String taskId);

}
